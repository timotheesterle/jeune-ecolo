const express = require('express');
const app = express();
const mysql = require('mysql');
const bodyParser = require('body-parser');

const urlencodedParser = bodyParser.urlencoded({ extended: false });

app.use(express.static(__dirname + '/views'));

app.set('view engine', 'ejs');

app.listen(process.env.PORT || 3000, () => console.log('listening'));

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'MySQL_123',
    database: 'impact'
});

connection.connect(err => {
    if (err) {
        console.error('Erreur de connexion : ' + err.stack);
        return;
    }

    console.log('Connexion n°' + connection.threadId);
});

let loggedUser = '';
let isAdmin = 0;

app.get('/', (req, res) => {
    if (loggedUser) {
        console.log(loggedUser.length);
        res.render('pages/accueil_connecte', {
            title: 'Accueil',
            name: loggedUser.name,
            loggedUser: loggedUser,
            isAdmin: isAdmin
        });
    } else {
        res.render('pages/accueil_visiteur', {
            title: 'Accueil',
            loggedUser: loggedUser,
            isAdmin: isAdmin
        });
    }
});

app.get('/inscription', (req, res) => {
    res.render('pages/inscription', {
        title: 'Inscription',
        loggedUser: loggedUser,
        isAdmin: isAdmin
    });
});

app.get('/connexion', (req, res) => {
    res.render('pages/connexion', {
        title: 'Connexion',
        loggedUser: loggedUser,
        isAdmin: isAdmin
    });
});

app.post('/inscription_validation', urlencodedParser, (req, res) => {
    let sql = 'INSERT INTO users (name, email, password) VALUES (?, ?, ?)';
    const values = [req.body.name, req.body.email, req.body.password];
    console.log(values);
    sql = mysql.format(sql, values);
    connection.query(sql, (error, results, fields) => {
        if (
            req.body.name !== '' &&
            req.body.email !== '' &&
            req.body.password !== ''
        ) {
            if (!error) {
                res.redirect('/');
            } else {
                console.log(error);
                res.send(error);
            }
        } else {
            res.render('pages/inscription', {
                title: 'Inscription',
                loggedUser: loggedUser,
                isAdmin: isAdmin
            });
        }
    });
});

app.post('/login', urlencodedParser, (req, res) => {
    let sql =
        'SELECT name, email, password, admin FROM users WHERE name = (?);';
    const values = req.body.name;
    sql = mysql.format(sql, values);
    connection.query(sql, (error, results, fields) => {
        console.log(results[0]);
        if (!error) {
            const message = 'Pas de correspondance trouvée en base';
            if (!results) {
                res.send(message);
            } else {
                if (req.body.password === results[0]['password']) {
                    loggedUser = {
                        name: results[0]['name'],
                        email: results[0]['email']
                    };
                    isAdmin = results[0]['admin'];
                    res.redirect('/');
                } else {
                    res.send(message);
                }
            }
        } else {
            console.log(error);
            res.send(error);
        }
    });
});

app.get('/gestion_utilisateurs', urlencodedParser, (req, res) => {
    function templateHTMLUser(id, name, email, admin) {
        return `
<tr>
    <td>${id}</td>
    <td>${name}</td>
    <td>${email}</td>
    <td>${admin === 0 ? 'Non' : 'Oui'}</td>
    <td>
        <form action="/user/delete/${id}" method="POST">
        <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">Supprimer</button>
        <form>
    </td>
</tr>
`;
    }

    let html = '';

    let sql = 'SELECT * FROM users;';
    connection.query(sql, (error, results, fields) => {
        if (!error) {
            results.forEach(
                user =>
                    (html += templateHTMLUser(
                        user.id,
                        user.name,
                        user.email,
                        user.admin
                    ))
            );
            res.render('pages/gestion_utilisateurs', {
                title: 'Gestion utilisateurs',
                loggedUser: loggedUser,
                isAdmin: isAdmin,
                userlist: html
            });
        } else {
            console.log(error);
            res.send(error);
        }
    });
});

app.post('/user/delete/:id', urlencodedParser, (req, res) => {
    let sql = 'DELETE FROM users WHERE id=' + req.params.id;
    connection.query(sql, (error, results, fields) => {
        if (!error) {
            res.redirect('/gestion_utilisateurs');
        } else {
            console.log(error);
            res.send(error);
        }
    });
});
